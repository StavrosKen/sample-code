angular.module('StavrosApp')

.factory('MultiLanguage', ['$q', '$rootScope', 'Restangular',
    function($q, $rootScope, Restangular) {

        var translationFiles = {},
            availableLocales = [],
            defaultLanguage  = {
                localeCode : "en_GB"
            },
            currentLanguage  = {
                localeCode : "en_GB"
            };

        var getLocales = function(){

            var deferred = $q.defer();
            
            Restangular.all('configuration/locales').getList({
                q: {
                    "status": 1
                }, 
                order: {
                    "isDefault": "DESC"
                }
            }).then(function(response){

                availableLocales = response;
                setDefaultLanguage();
                deferred.resolve(true);    

            },function(){

                console.log("couldnt fetch remote locales");
                deferred.reject(false);
            });       

            return deferred.promise;
        }

        var getTranslationFiles = function(){

            var deferred = $q.defer();
            
            Restangular.all('/translation-files').customGET("")
            .then(function(response){

                translationFiles = response;
                deferred.resolve(true);  

            },function(){

                getFallBackTranslationFile();
                console.log("couldnt fetch remote translation files");
                deferred.reject(false);
            });       

            return deferred.promise;
        }

        var getFallBackTranslationFile = function(){


            console.log("got alternative language file");

        }

        var setDefaultLanguage = function(){

            defaultLanguage = availableLocales[0];
            currentLanguage = availableLocales[0];
        }

        return {

            init: (function() {

                var deferred = $q.defer();

                getLocales().then(function(){

                    getTranslationFiles().then(function(){
                        deferred.resolve("everything fetched");
                    },function(){

                        deferred.reject("translation files error");
                    });
                },function(){

                    deferred.reject("locales error");
                });

                    return deferred.promise;
                }()),
            
            translate: function(string){

                var translationOfString = translationFiles[currentLanguage.localeCode][string];

                if (!translationOfString){

                    translationOfString = translationFiles[defaultLanguage.localeCode][string];
                    console.warn("String '" + string + "' is missing a " + currentLanguage.name + " translation.");
                }

                return translationOfString;
            },

            selectedLanguage: function(){

                return currentLanguage;
            },

            defaultLanguage: function(){

                return defaultLanguage;
            },

            translationFiles:function(){

                return translationFiles;
            },

            availableLocales:function(){

                return availableLocales;
            },

            changeLanguage: function(lang){

                currentLanguage = lang;
                $rootScope.$broadcast('language-changed'); 
            }

            }

        }]);
