'use strict';

angular.module('StavrosApp')
  .controller('ProfileCtrl',['$scope','$rootScope','Restangular', function ($scope, $rootScope, Restangular) {

    $scope.profileData          = $rootScope.User ;
    $scope.profileActiveSection = 0;
    $scope.emailChange          = {};
    $scope.passwordChange       = {};
    $scope.profileSections      = [
        "Personal Details",
        "Location Details", 
        "Basic Info",
        "Academic History",
        "Security",
        "Settings"
    ];
    $scope.interests            = [
        'Technology', 
        'Development', 
        'Programming', 
        'Coding'
    ];

    //--- Submit Funcitons

    $scope.detailsFunc = function(){
    var detailsChangeRequest  = Restangular.one("/user").post(null, $scope.profileData)
            .then(function(userObj) {
                $scope.apiMessage = "Your changes have been submitted";
            });
    };

    $scope.emailFunc = function(){
    var emailChangeRequest  = Restangular.one("/security/change-username").post(null, $scope.emailChange)
            .then(function(userObj) {
                $scope.apiMessage = "Your changes have been submitted";
            });
    };
     $scope.passwordFunc = function(){
    var passwordChangeRequest  = Restangular.one("/security/change-password").post(null, $scope.passwordChange)
            .then(function(userObj) {
                $scope.apiMessage = "Your changes have been submitted";
            });
    };
    
    $scope.changeActive = function(ActiveStep){
    	$scope.profileActiveSection = ActiveStep;
    };
    
  //--- Show Form Errors

    $scope.firstNameError = function(){
        if(!this.personalDetailsForm.firstName.$pristine){
            if((this.personalDetailsForm.firstName.$invalid && !this.personalDetailsForm.firstName.$pristine && !this.personalDetailsForm.firstName.$error.pattern) || this.personalDetailsForm.firstName.$error.minlength){
                return 'Your first name should be at least 2 characters long.';
            }
            else if((this.personalDetailsForm.firstName.$error.pattern && !this.personalDetailsForm.firstName.$pristine) && !this.personalDetailsForm.firstName.$error.minlength){
                return 'Your first name must contain only letters.';
            }
            return false;
        }
    };

    $scope.middleNamesError = function(){
    if(!this.personalDetailsForm.middleNames.$pristine){
        if((this.personalDetailsForm.middleNames.$error.pattern && !this.personalDetailsForm.middleNames.$pristine) && !this.personalDetailsForm.middleNames.$error.minlength){
            return 'Your middle names must contain only letters.';
        }
        return false;
    }
  };

   $scope.lastNameError = function(){
        if(!this.personalDetailsForm.lastName.$pristine){
            if((this.personalDetailsForm.lastName.$invalid && !this.personalDetailsForm.lastName.$pristine && !this.personalDetailsForm.lastName.$error.pattern) || this.personalDetailsForm.lastName.$error.minlength){
                return 'Your last name should be at least 2 characters long.';
            }
            else if((this.personalDetailsForm.lastName.$error.pattern && !this.personalDetailsForm.lastName.$pristine) && !this.personalDetailsForm.lastName.$error.minlength){
                return 'Your last name must contain only letters.';
            }
            return false;
        }
    };

  }])


.controller('PublicProfileCtrl',['$scope','$rootScope','Restangular', '$routeParams', function ($scope, $rootScope, Restangular, $routeParams) {
    $scope.user=$rootScope.User;
    Restangular.one('user', $routeParams.id).get().then(function(theuser){
        $scope.theUser = theuser;
    });
    
  }]);
