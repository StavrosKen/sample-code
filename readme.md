### Stavros Kainich Frontend Code###
Multilanguage Angular Plugin

#### Multilanguage Angular Plugin ####
**Multilanguage factory**
**Translate Filter**
**Multilanguage Directive**
These components include the main functionality of the plugin.

To trigger the change language functionality though, 
we ll need a header bar (or just template-controller) including a dropdown with the available languages
that trigger the change.

including in the template : 
 <select ng-model="lang" ng-change="changeLanguage()" ng-options="locale.name for locale in availableLocales"></select>
and in the controller : 
scope.availableLocales = MultiLanguage.availableLocales();
scope.lang = scope.availableLocales[0];
scope.changeLanguage = function() {
    MultiLanguage.changeLanguage(scope.lang);
};



#### Angular Simple Profile ####
**profile.controller.js**
**profile.view.html**
Simple template - controller implementation of forms